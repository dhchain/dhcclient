package org.dhc.gui.post;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import org.dhc.gui.Caller;
import org.dhc.gui.Main;
import org.dhc.util.DhcAddress;

public class MyRatings extends AbstractAction implements Caller {

	private static final long serialVersionUID = 4036313657721664495L;
	
	private Main main;
	
	public MyRatings(Main main) {
		putValue(NAME, "My Ratings");
		putValue(SHORT_DESCRIPTION, "My Ratings");
		this.main = main;
	}
	
	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}

	public void actionPerformed(ActionEvent actionEvent) {
		new RatingHelper(main).showRater(DhcAddress.getMyDhcAddress().getAddress());
	}
	

	

}
