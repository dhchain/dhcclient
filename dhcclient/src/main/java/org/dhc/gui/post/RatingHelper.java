package org.dhc.gui.post;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.dhc.gui.Main;
import org.dhc.gui.message.SendMessage;
import org.dhc.gui.util.JTextFieldRegularPopupMenu;
import org.dhc.gui.util.SwingUtil;
import org.dhc.gui.util.TableColumnAdjuster;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.lite.post.Ratee;
import org.dhc.lite.post.Rating;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.StringUtil;
import org.dhc.util.ThreadExecutor;

import com.jidesoft.swing.AutoResizingTextArea;

import net.miginfocom.swing.MigLayout;

public class RatingHelper {
	
	private static final DhcLogger logger = DhcLogger.getLogger();

	private Main main;

	public RatingHelper(Main main) {
		this.main = main;
	}
	
	public void showRatee(final String ratee, final String transactionId) {
		final JLabel label = SwingUtil.showWait(main);

		ThreadExecutor.getInstance().execute(new DhcRunnable("showRater") {
			public void doRun() {
				doShowRatee(ratee, transactionId, label);
			}
		});
		
	}

	private void doShowRatee(String account, String transactionId, JLabel label) {

		Ratee ratee = DhcLiteHelper.getRatee(CryptoUtil.getDhcAddressFromString(account), transactionId);
		
		if(ratee == null) {
			label.setText("Failed to retrieve ratee. Please try again");
			return;
		}
		show(ratee);
		
	}
	
	public void show(final Ratee ratee) {
		JPanel jpanel = new JPanel(new MigLayout("wrap 2", "[right][fill]"));
		JScrollPane scrollPane = new JScrollPane(jpanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		jpanel.add(new JLabel("Post:"));
		
		JTextField account = new JTextField(45);
		account.setText(ratee.getName());
		account.setOpaque(false);
		account.setEditable(false);
		account.setBorder(null);
		JTextFieldRegularPopupMenu.addTo(account);
		account.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
            	if(SwingUtilities.isLeftMouseButton(e)) {
            		SendMessage sendMessage = new SendMessage(main, ratee.getCreatorDhcAddress());
            		sendMessage.setSubject("Re: " + ratee.getName());
            		sendMessage.actionPerformed(null);
            	}
            }
        });
		account.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		account.setForeground(Color.BLUE);
		Map<TextAttribute, Integer> attributes = new HashMap<>();
		attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		account.setFont(account.getFont().deriveFont(attributes));
		jpanel.add(account);

		jpanel.add(new JLabel("Description:"));
		
		JTextArea description = new AutoResizingTextArea(1, 40, 45);
		description.setText(ratee.getDescription());
		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		description.setOpaque(false);
		description.setEditable(false);
		description.revalidate();
		jpanel.add(description);
		
		jpanel.add(new JLabel("Created On:"));
		
		JTextField date = new JTextField(45);
		date.setText(new Date(ratee.getTimeStamp()).toString());
		date.setOpaque(false);
		date.setEditable(false);
		date.setBorder(null);
		jpanel.add(date);
		
		jpanel.add(new JLabel("Identifier:"));
		
		JTextField transactionId = new JTextField(45);
		transactionId.setText(ratee.getTransactionId());
		transactionId.setOpaque(false);
		transactionId.setEditable(false);
		transactionId.setBorder(null);
		jpanel.add(transactionId);
		
		jpanel.add(new JLabel("Comment:"));
		
		JTextArea comment = new AutoResizingTextArea(5, 40, 45);
		comment.setLineWrap(true);
		comment.setWrapStyleWord(true);
		JTextFieldRegularPopupMenu.addTo(comment);
		comment.setBorder(new JTextField().getBorder());
		jpanel.add(comment);
		
		jpanel.add(new JLabel("Rating:"));
		
        //... Create the buttons.
        JRadioButton yesButton   = new JRadioButton("Yes");
        JRadioButton noButton    = new JRadioButton("No");

        //... Create a button group and add the buttons.
        ButtonGroup bgroup = new ButtonGroup();
        bgroup.add(yesButton);
        bgroup.add(noButton);
        
        
        //... Arrange buttons vertically in a panel
        JPanel radioPanel = new JPanel();
        radioPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        radioPanel.add(yesButton);
        radioPanel.add(noButton);
        
        //... Add a titled border to the button panel.
        radioPanel.setBorder(BorderFactory.createTitledBorder(
                   BorderFactory.createEtchedBorder(), "Approve?"));
        
        jpanel.add(radioPanel);
        
        jpanel.add(new JLabel(""));
        
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setAction(new AddRatingAction(main, comment, bgroup, account, transactionId));
		p.add(btnSubmit);
		jpanel.add(p);
		
		showComments(jpanel, ratee);
		
		jpanel.add(new JSeparator(), "growx, span");
		
		String myTmaAdress = DhcAddress.getMyDhcAddress().getAddress();
		
		if(myTmaAdress.equals(ratee.getCreatorDhcAddress())) {
			jpanel.add(new JLabel(""));
	        
	        p = new JPanel(new FlowLayout(FlowLayout.LEFT));
			JButton deleteRatee = new JButton("Delete Post");
			deleteRatee.setAction(new DeleteRateeAction(main, ratee.getTransactionId(), ratee.getName()));
			p.add(deleteRatee);
			jpanel.add(p);
		}
		
		JFrame frame = main.getFrame();
		frame.getContentPane().removeAll();
		frame.getContentPane().add(scrollPane);
		frame.revalidate();
		frame.getContentPane().repaint();
		
	}
	
	private void showComments(JPanel form, Ratee ratee) {
		String accountName = StringUtil.trim(ratee.getName());
		List<Rating> list = DhcLiteHelper.searchRatings(accountName, ratee.getTransactionId());
		if(list == null) {
			form.add(new JLabel("Failed to retrieve ratings. Please try again"), "span, left");
			return;
		}
		
		
		form.add(new JLabel("Total rating is " + ratee.getTotalRating()), "span, left");
		
		
		
		showRatings(list, form);
		
	}
	
	private void showRatings(List<Rating> list, JComponent form) {
		for(final Rating rating: list) {
			
			form.add(new JSeparator(), "growx, span");
			
			form.add(new JLabel("Rater:"));
			
			JTextField rater = new JTextField(45);
			rater.setText(rating.getRater());
			rater.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			rater.setOpaque(false);
			rater.setEditable(false);
			rater.setBorder(null);
			rater.setForeground(Color.BLUE);
			Font font = rater.getFont();
			Map<TextAttribute, Integer> attributes = new HashMap<>();
			attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
			rater.setFont(font.deriveFont(attributes));
			rater.addMouseListener(new MouseAdapter(){
	            public void mouseClicked(MouseEvent e){
	            	if(SwingUtilities.isLeftMouseButton(e)) {
	            		showRater(rating.getRater());
	            	}
	            }
	        });
			form.add(rater);
			
			form.add(new JLabel("Post:"));
			
			JTextField ratee = new JTextField(45);
			ratee.setText(rating.getRatee());
			ratee.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			ratee.setOpaque(false);
			ratee.setEditable(false);
			ratee.setBorder(null);
			ratee.setForeground(Color.BLUE);
			JTextFieldRegularPopupMenu.addTo(ratee);
			ratee.setFont(font.deriveFont(attributes));
			ratee.addMouseListener(new MouseAdapter(){
	            public void mouseClicked(MouseEvent e){
	            	if(SwingUtilities.isLeftMouseButton(e)) {
	            		showRatee(rating.getRatee(), rating.getTransactionId());
	            	}
	                
	            }
	        });
			form.add(ratee);
			
			form.add(new JLabel("Rate:"));
			
			JTextField rate = new JTextField(45);
			rate.setText(rating.getDisplayRate());
			rate.setOpaque(false);
			rate.setEditable(false);
			rate.setBorder(null);
			JTextFieldRegularPopupMenu.addTo(rate);
			form.add(rate);
			
			form.add(new JLabel("Date:"));
			
			JTextField date = new JTextField(45);
			date.setText(new Date(rating.getTimeStamp()).toString());
			date.setOpaque(false);
			date.setEditable(false);
			date.setBorder(null);
			JTextFieldRegularPopupMenu.addTo(date);
			form.add(date);
			
			form.add(new JLabel("Comment:"));
			
			JTextArea comment = new AutoResizingTextArea(1, 40, 45);
			comment.setText(rating.getComment());
			comment.setLineWrap(true);
			comment.setWrapStyleWord(true);
			comment.setOpaque(false);
			comment.setEditable(false);
			comment.revalidate();
			form.add(comment);

		}
	}
	
	public void showRater(final String rater) {
		final JLabel label = SwingUtil.showWait(main);

		ThreadExecutor.getInstance().execute(new DhcRunnable("showRater") {
			public void doRun() {
				displayRaterComments(rater, label);
			}
		});
		
	}

	private void displayRaterComments(final String rater, final JLabel label) {
		List<Rating> list = DhcLiteHelper.searchRatingsForRater(rater);
		
		if(list == null) {
			label.setText("Failed to retrieve ratings. Please try again");
			return;
		}
		
		JPanel form = new JPanel();
		
		form.setLayout(new BoxLayout(form, BoxLayout.Y_AXIS));

		JPanel jpanel = new JPanel(new MigLayout("wrap 2", "[right][fill]"));
		JScrollPane scrollPane = new JScrollPane(jpanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		form.add(scrollPane);
		
		jpanel.add(new JLabel("Total number of comments found for " + rater + ": " + list.size()), "span, left");
		
		showRatings(list, jpanel);
	
		JFrame frame = main.getFrame();
		frame.getContentPane().removeAll();
		frame.getContentPane().add(form);
		frame.revalidate();
		frame.getContentPane().repaint();
	}

	public void showPosts(final String tmaAddress) {
		final JLabel label = SwingUtil.showWait(main);

		ThreadExecutor.getInstance().execute(new DhcRunnable("showRater") {
			public void doRun() {
				doShowPosts(tmaAddress, label);
			}
		});
		
	}

	private void doShowPosts(String tmaAddress, JLabel label) {
		List<Ratee> list = DhcLiteHelper.getPosts(new DhcAddress(tmaAddress));
		
		if(list == null) {
			label.setText("Failed to retrieve posts. Please try again");
			return;
		}
		
		logger.debug("Retrieved {} posts", list.size());
		
		if(list.size() == 0) {
			label.setText("No posts were found.");
			return;
		}
		
		displayPosts(list, label);
		
	}
	
	public void displayPosts(List<Ratee> list, JLabel label) {
		JPanel form = new JPanel();
		BoxLayout boxLayout = new BoxLayout(form, BoxLayout.Y_AXIS);
		form.setLayout(boxLayout);
		
		label = new JLabel("Found " + list.size() + " posts: ");
		label.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		label.setBorder(new EmptyBorder(5,5,5,5));
		form.add(label);
		
		RateeTableModel model = new RateeTableModel(list);
		JTable table = new JTable(model);

		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		TableColumnAdjuster tca = new TableColumnAdjuster(table);
		tca.adjustColumns();
		table.addMouseListener(new RateeMouseAdapter(table, list, main));
		
		JScrollPane scroll = new JScrollPane (table);
		scroll.setBorder(null);
		scroll.setAlignmentX(JScrollPane.LEFT_ALIGNMENT);
		form.add(scroll);
		
		JFrame frame = main.getFrame();
		frame.getContentPane().removeAll();
		frame.getContentPane().add(form);
		frame.revalidate();
		frame.getContentPane().repaint();
	}

}
