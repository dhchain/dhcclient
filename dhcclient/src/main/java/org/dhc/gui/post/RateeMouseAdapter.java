package org.dhc.gui.post;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;

import org.dhc.gui.Main;
import org.dhc.gui.util.SwingUtil;
import org.dhc.lite.post.Ratee;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.ThreadExecutor;

public class RateeMouseAdapter extends MouseAdapter {
	
	private static final DhcLogger logger = DhcLogger.getLogger();
	
	private JTable table;
	private List<Ratee> list;
	private Main main;
	
	public RateeMouseAdapter(JTable table, List<Ratee> list, Main main) {
		this.table = table;
		this.list = list;
		this.main = main;
	}
	
	public void mouseClicked(MouseEvent evt) {
        int row = table.rowAtPoint(evt.getPoint());
        int col = table.columnAtPoint(evt.getPoint());
        if (row >= 0 && col >= 0) {
            doit(list.get(row));
        }
    }

	private void doit(final Ratee ratee) {
		final JLabel label = SwingUtil.showWait(main);

		ThreadExecutor.getInstance().execute(new DhcRunnable("RateeMouseAdapter") {
			public void doRun() {
				label.setText(ratee.getName());
				logger.debug("ratee.getName()={}", ratee.getName());
				
				new RatingHelper(main).show(ratee);
				
			}

			
		});
	}
	
}
