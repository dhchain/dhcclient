package org.dhc.gui.post;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.dhc.gui.Caller;
import org.dhc.gui.Main;
import org.dhc.gui.util.JTextFieldRegularPopupMenu;
import org.dhc.gui.util.SwingUtil;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.StringUtil;
import org.dhc.util.ThreadExecutor;

import com.jidesoft.swing.AutoResizingTextArea;

import net.miginfocom.swing.MigLayout;

public class AddRatingAction extends AbstractAction implements Caller {

	private static final long serialVersionUID = -6540143195727094951L;
	private static final DhcLogger logger = DhcLogger.getLogger();
	
	private Main main;
	private JTextArea comment;
	private ButtonGroup bgroup;
	private JTextField account;
	private JTextField transactionId;
	
	public AddRatingAction(Main main, JTextArea comment, ButtonGroup bgroup, JTextField account, JTextField transactionId) {
		putValue(NAME, "Add Rating");
		putValue(SHORT_DESCRIPTION, "Add Rating");
		this.main = main;
		this.comment = comment;
		this.bgroup = bgroup;
		this.account = account;
		this.transactionId = transactionId;
		
	}

	public void actionPerformed(ActionEvent e) {
		
		if(SwingUtil.getSelectedButtonText(bgroup) == null) {
			log("Please click on Yes or No radio button for rating.");
			return;
		}
		if(StringUtil.isEmpty(comment.getText())) {
			log("Please enter comment.");
			return;
		}
		
		final JLabel label = SwingUtil.showWait(main);

		ThreadExecutor.getInstance().execute(new DhcRunnable("AddRatingAction") {
			public void doRun() {
				logger.debug("comment: {}", comment.getText());
				logger.debug("selected button: {}", SwingUtil.getSelectedButtonText(bgroup));
				if(doIt(label)) {
					feedback();
				}
			}

			
		});
	}

	private void feedback() {
		
		JPanel form = new JPanel();
		form.setLayout(new BoxLayout(form, BoxLayout.Y_AXIS));
		
		JPanel jpanel = new JPanel(new MigLayout("wrap 2", "[right][fill]"));
		JScrollPane scrollPane = new JScrollPane(jpanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		form.add(scrollPane);
		
		String rating = "Yes".equals(SwingUtil.getSelectedButtonText(bgroup)) ? "Positive": "Negative";
		jpanel.add(new JLabel(rating + " rating was added"), "span, left");
		
		jpanel.add(new JLabel("Post:"));
		
		JTextField ratee = new JTextField(45);
		ratee.setText(account.getText());
		ratee.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		ratee.setOpaque(false);
		ratee.setEditable(false);
		ratee.setBorder(null);
		ratee.setForeground(Color.BLUE);
		JTextFieldRegularPopupMenu.addTo(ratee);
		Map<TextAttribute, Integer> attributes = new HashMap<>();
		attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		ratee.setFont(ratee.getFont().deriveFont(attributes));
		ratee.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
            	if(SwingUtilities.isLeftMouseButton(e)) {
            		new RatingHelper(main).showRatee(account.getText(), transactionId.getText());
            	}
                
            }
        });
		jpanel.add(ratee);
		
		jpanel.add(new JLabel("Comment:"));
		
		JTextArea description = new AutoResizingTextArea(5, 40, 45);
		description.setText(comment.getText());
		
		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		description.setOpaque(false);
		description.setEditable(false);
		description.revalidate();
		
		jpanel.add(description);
		
		JFrame frame = main.getFrame();
		frame.getContentPane().removeAll();
		frame.getContentPane().add(form);
		frame.revalidate();
		frame.getContentPane().repaint();
		
	}

	private boolean doIt(JLabel label) {
		String accountName = account.getText().trim();
		String result = DhcLiteHelper.addRating(accountName, transactionId.getText().trim(), comment.getText().trim(), SwingUtil.getSelectedButtonText(bgroup));
		if(result != null) {
			label.setText(result);
			return false;
		}
		
		JFrame frame = main.getFrame();
		frame.revalidate();
		frame.getContentPane().repaint();
		return true;
	}

	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}
	


}
