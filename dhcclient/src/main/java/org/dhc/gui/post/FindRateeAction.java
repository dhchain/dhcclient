package org.dhc.gui.post;

import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.dhc.gui.Caller;
import org.dhc.gui.Main;
import org.dhc.gui.util.SwingUtil;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.lite.post.Ratee;
import org.dhc.util.DhcRunnable;
import org.dhc.util.StringUtil;
import org.dhc.util.ThreadExecutor;

public class FindRateeAction extends AbstractAction implements Caller {

	private static final long serialVersionUID = 6886690569988480986L;
	
	private Main main;
    private JTextField account;
    private JTextField jkeywords;
	
	public FindRateeAction(Main main, JTextField account, JTextField jkeywords) {
		putValue(NAME, "Find Post");
		putValue(SHORT_DESCRIPTION, "Find Post");
		this.main = main;
		this.account = account;
		this.jkeywords = jkeywords;
		
	}

	public void actionPerformed(ActionEvent e) {
		final JLabel label = SwingUtil.showWait(main);
		ThreadExecutor.getInstance().execute(new DhcRunnable("FindRateeAction") {
			public void doRun() {
				findRatees(label);
			}
		});
	}
	
	
	private void findRatees(JLabel label) {
		Set<String> words = DhcLiteHelper.getKeywords(jkeywords.getText());
		String accountName = StringUtil.trim(account.getText());
		
		List<Ratee> list = DhcLiteHelper.searchRatees(accountName, words);
		
		if(list == null) {
			label.setText("Failed to retrieve posts. Please try again");
			return;
		}
		
		if(list.size() == 0) {
			label.setText("No posts were found for provided keywords.");
			return;
		}
		
		
		new RatingHelper(main).displayPosts(list, label);
		
	}
	
	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}

}
