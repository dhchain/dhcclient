package org.dhc.gui.post;

import java.awt.event.ActionEvent;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.dhc.blockchain.Keywords;
import org.dhc.blockchain.Transaction;
import org.dhc.gui.Caller;
import org.dhc.gui.Main;
import org.dhc.gui.util.SwingUtil;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.util.DhcRunnable;
import org.dhc.util.StringUtil;
import org.dhc.util.ThreadExecutor;

public class CreateRateeAction extends AbstractAction implements Caller {

	private static final long serialVersionUID = 6886690569988480986L;
	
    private Main main;
    private JTextField account;
    private JTextArea description;
    private JTextField jkeywords;
	
	public CreateRateeAction(Main main, JTextField account, JTextArea description, JTextField jkeywords) {
		putValue(NAME, "Create Post");
		putValue(SHORT_DESCRIPTION, "Create Post");
		this.main = main;
		this.account = account;
		this.description = description;
		this.jkeywords = jkeywords;
		
	}

	public void actionPerformed(ActionEvent e) {
		
		if(StringUtil.isEmpty(account.getText())) {
			log("Post title can not be blank");
			return;
		}
		if(account.getText().length() > Keywords.KEYWORD_MAX_LENGTH) {
			log("Post title can not be longer than " + Keywords.KEYWORD_MAX_LENGTH + " characters");
			return;
		}
		if(StringUtil.isEmpty(description.getText())) {
			log("Description can not be blank");
			return;
		}
		if(StringUtil.isEmpty(jkeywords.getText())) {
			log("Keywords can not be blank");
			return;
		}
		
		final JLabel label = SwingUtil.showWait(main);
		
		ThreadExecutor.getInstance().execute(new DhcRunnable("CreateRateeAction") {
			public void doRun() {
				if(sendCreateRateeTransaction(label) != null) {
					label.setText("Post " + account.getText() + " was created successfully with keywords: " + DhcLiteHelper.getKeywords(jkeywords.getText()));
				}
			}
		});

		


	}
	
	
	private Transaction sendCreateRateeTransaction(JLabel label) {
		Set<String> words = DhcLiteHelper.getKeywords(jkeywords.getText());
		String accountName = account.getText().trim();
		Transaction transaction = DhcLiteHelper.sendCreateRateeTransaction(accountName, words, description.getText());
		if(transaction == null) {
			label.setText("Could not create post");
		}
		return transaction;
	}
	
	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}

}
