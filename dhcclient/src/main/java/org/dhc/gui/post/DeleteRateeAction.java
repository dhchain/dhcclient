package org.dhc.gui.post;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.dhc.gui.Caller;
import org.dhc.gui.Main;
import org.dhc.gui.util.SwingUtil;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.ThreadExecutor;

public class DeleteRateeAction extends AbstractAction implements Caller {

	private static final long serialVersionUID = -6540143195727094951L;
	private static final DhcLogger logger = DhcLogger.getLogger();
	
	private Main main;
	private String transactionId;
	private String rateeName;
	
	public DeleteRateeAction(Main main, String transactionId, String rateeName) {
		putValue(NAME, "Delete Post");
		putValue(SHORT_DESCRIPTION, "Delete Post");
		this.main = main;
		this.transactionId = transactionId;
		this.rateeName = rateeName;
	}

	public void actionPerformed(ActionEvent e) {
		int input = JOptionPane.showConfirmDialog(main.getFrame(), 
                "Are you sure you want to delete Post? This action cannot be undone!", "Confirm Post Deletion", 
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);

		if(input != JOptionPane.OK_OPTION) {
			return;
		}
		
		final JLabel label = SwingUtil.showWait(main);

		ThreadExecutor.getInstance().execute(new DhcRunnable("DeleteRateeAction") {
			public void doRun() {
				doIt(label);
			}
		});
	}

	private void doIt(JLabel label) {
		logger.debug("transactionId={}", transactionId);
		
		sendDeleteRateeTransaction(label);
		
		
	}

	private void sendDeleteRateeTransaction(JLabel label) {
		String result = DhcLiteHelper.sendDeleteRateeTransaction(rateeName, transactionId);
		if(result != null) {
			label.setText(result);
		} else {
			label.setText("Post with identifier " + transactionId + " was deleted.");
		}
	}

	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}

}
