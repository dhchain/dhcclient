package org.dhc.gui.post;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import org.dhc.gui.Caller;
import org.dhc.gui.Main;
import org.dhc.util.DhcAddress;

public class MyPosts extends AbstractAction implements Caller {

	private static final long serialVersionUID = 4036313657721664495L;
	
	private Main main;
	
	public MyPosts(Main main) {
		putValue(NAME, "My Posts");
		putValue(SHORT_DESCRIPTION, "My Posts");
		this.main = main;
	}
	
	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}

	public void actionPerformed(ActionEvent actionEvent) {
		new RatingHelper(main).showPosts(DhcAddress.getMyDhcAddress().getAddress());
	}
	

	

}
