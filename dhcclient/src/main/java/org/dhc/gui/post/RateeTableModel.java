package org.dhc.gui.post;

import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.dhc.lite.post.Ratee;

public class RateeTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1012645969055258357L;
	
	private List<Ratee> list;
	
	public RateeTableModel(List<Ratee> list) {
		this.list = list;
	}

	public int getRowCount() {
		return list.size();
	}

	public int getColumnCount() {
		return 4;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Ratee ratee = list.toArray(new Ratee[0])[rowIndex];
        Object value = "";
        switch (columnIndex) {
            case 0:
                value = ratee.getName();
                break;
            case 1:
            	value = ratee.getDescription().length() > 50? ratee.getDescription().substring(0, 50) + " ...": ratee.getDescription();
    			break;
            case 2:
            	value = new Date(ratee.getTimeStamp()).toString();
    			break;
            case 3:
            	value = ratee.getTotalRating();
    			break;
        }
        return value;
	}

	public String getColumnName(int columnIndex) {
		String value = null;
		switch (columnIndex) {
		case 0:
			value = "Post";
			break;
		case 1:
			value = "Description";
			break;
		case 2:
			value = "Created on";
			break;
		case 3:
			value = "Rating";
			break;
		}
		return value;
	}

	

}
