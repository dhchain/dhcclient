package org.dhc.gui.post;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.dhc.gui.Caller;
import org.dhc.gui.Main;
import org.dhc.gui.util.JTextFieldRegularPopupMenu;

import net.miginfocom.swing.MigLayout;

public class FindRatee extends AbstractAction implements Caller {

	private static final long serialVersionUID = 4036313657721664495L;
	
	private Main main;
	
	public FindRatee(Main main) {
		putValue(NAME, "Find Post");
		putValue(SHORT_DESCRIPTION, "Find Post");
		this.main = main;
	}
	
	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}

	public void actionPerformed(ActionEvent actionEvent) {
		JPanel form = createForm();
		JFrame frame = main.getFrame();
		frame.getContentPane().removeAll();
		frame.getContentPane().add(form);
		frame.revalidate();
		frame.getContentPane().repaint();
	}
	
	private JPanel createForm() {
		JPanel form = new JPanel(new MigLayout(
		        "wrap 2",
		        "[right][fill]"
		        ));
		
		form.add(new JLabel("Post:"));
		
		JTextField account = new JTextField(45);
		JTextFieldRegularPopupMenu.addTo(account);
		form.add(account);

		form.add(new JLabel("Keywords:"));
		
		JTextField keywords = new JTextField(45);
		JTextFieldRegularPopupMenu.addTo(keywords);
		keywords.setToolTipText("Enter list of words separated by blank spaces that will be used to find posts");
		form.add(keywords);
		
		form.add(new JLabel(""));
		
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setAction(new FindRateeAction(main, account, keywords));
		p.add(btnSubmit);
		form.add(p);

		main.getFrame().getRootPane().setDefaultButton(btnSubmit);
		
		return form;
	}
	

}
