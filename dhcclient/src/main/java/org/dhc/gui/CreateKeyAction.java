package org.dhc.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.dhc.util.DhcLogger;
import org.dhc.util.PasswordHelper;

public class CreateKeyAction extends AbstractAction {
	
	private static final long serialVersionUID = -5348678702516608164L;
	private static final DhcLogger logger = DhcLogger.getLogger();

	private JLabel label;
	private Main main;
	
	public CreateKeyAction(Main main) {
		putValue(NAME, "Submit");
		putValue(SHORT_DESCRIPTION, "Submit Passphrase");
		this.main = main;
	}

	public void actionPerformed(ActionEvent actionEvent) {
		String passphrase = new String(main.getPasswordField().getPassword());
		String confirmPassword = new String(main.getConfirmPasswordField().getPassword());
		if(!passphrase.equals(confirmPassword)) {
			String message = "Passphrase and reentered passphrase do not match";
			JOptionPane.showMessageDialog(main.getFrame(), message);
			return;
		}
		try {
			PasswordHelper passwordHelper = new PasswordHelper(main.getKey());
			passwordHelper.generateKey(passphrase);
			
			
			JPanel form = new JPanel(new BorderLayout());
			label = new JLabel("New key generated, starting network");
			label.setAlignmentX(JLabel.LEFT_ALIGNMENT);
			label.setBorder(new EmptyBorder(5,5,5,5));
			form.add(label);
			
			JFrame frame = main.getFrame();
			frame.getContentPane().removeAll();
			frame.getContentPane().add(form, BorderLayout.NORTH);
			frame.revalidate();
			frame.getContentPane().repaint();
			
			main.start();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

}
