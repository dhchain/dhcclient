package org.dhc.gui.transaction;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.dhc.blockchain.Transaction;
import org.dhc.gui.Main;
import org.dhc.util.DhcLogger;

import net.miginfocom.swing.MigLayout;

public class MempoolMouseAdapter extends MouseAdapter {
	
	private static final DhcLogger logger = DhcLogger.getLogger();
	
	private JTable table;
	private List<Transaction> list;
	private Main main;
	
	public void mouseClicked(MouseEvent evt) {
        int row = table.rowAtPoint(evt.getPoint());
        int col = table.columnAtPoint(evt.getPoint());
        if (row >= 0 && col >= 0) {
            doit(list.get(row));
        }
    }
	
	private JPanel showBackButton() {
		JFrame frame = main.getFrame();
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		final JButton btnSubmit = new JButton();
		btnSubmit.setAction(new Mempool(main));
		btnSubmit.setText("Back");
		
		frame.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0), "backButton");

		frame.getRootPane().getActionMap().put("backButton", new AbstractAction() {
			private static final long serialVersionUID = 4946947535624344910L;

			public void actionPerformed(ActionEvent actionEvent) {
				btnSubmit.doClick();
				frame.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).clear();
				frame.getRootPane().getActionMap().clear();
			}
		});
		
		p.add(btnSubmit);
		return p;
	}

	private void doit(Transaction transaction) {
		
		logger.info("transaction {}", transaction);
		
		JPanel mainForm = new JPanel(new BorderLayout());
		
		JPanel form = new JPanel(new MigLayout("wrap 2", "[right][fill]"));
		mainForm.add(form, BorderLayout.NORTH);
		JScrollPane scrollPane = new JScrollPane(mainForm, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		JPanel p = showBackButton();
		
		form.add(p);
		form.add(new JPanel());
		
		JLabel label = new JLabel("Transaction ID:", JLabel.RIGHT);
		form.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField identifier = new JTextField(36);
		identifier.setText(transaction.getTransactionId());
		identifier.setBorder( null );
		identifier.setOpaque( false );
		identifier.setEditable( false );
		p.add(identifier);
		form.add(p);
		
		label = new JLabel("Blockhash:", JLabel.RIGHT);
		form.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField blockhash = new JTextField(36);
		blockhash.setText(transaction.getBlockHash());
		blockhash.setBorder( null );
		blockhash.setOpaque( false );
		blockhash.setEditable( false );
		p.add(blockhash);
		form.add(p);
		
		label = new JLabel("Block Index:", JLabel.RIGHT);
		form.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField blockIndex = new JTextField(36);
		blockIndex.setText(Long.toString(transaction.getBlockIndex()));
		blockIndex.setBorder( null );
		blockIndex.setOpaque( false );
		blockIndex.setEditable( false );
		p.add(blockIndex);
		form.add(p);
		
		label = new JLabel("Time Stamp:", JLabel.RIGHT);
		form.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField timeStamp = new JTextField(36);
		timeStamp.setText(new Date(transaction.getTimeStamp()).toString());
		timeStamp.setBorder( null );
		timeStamp.setOpaque( false );
		timeStamp.setEditable( false );
		p.add(timeStamp);
		form.add(p);
		
		label = new JLabel("Sender:", JLabel.RIGHT);
		form.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField address = new JTextField(36);
		address.setText(transaction.getSenderDhcAddress().toString());
		address.setBorder( null );
		address.setOpaque( false );
		address.setEditable( false );
		p.add(address);
		form.add(p);
		
		label = new JLabel("Recipient:", JLabel.RIGHT);
		form.add(label);
		
		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField recipient = new JTextField(36);
		recipient.setText(transaction.getReceiver().toString());
		recipient.setBorder( null );
		recipient.setOpaque( false );
		recipient.setEditable( false );
		p.add(recipient);
		form.add(p);
		
		
		label = new JLabel("Value:", JLabel.RIGHT);
		form.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField amount = new JTextField(36);
		amount.setText(transaction.getValue().toNumberOfCoins());
		amount.setBorder( null );
		amount.setOpaque( false );
		amount.setEditable( false );
		p.add(amount);
		form.add(p);
		
		label = new JLabel("Fee:", JLabel.RIGHT);
		form.add(label);
				
		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField fee = new JTextField(36);
		fee.setText(transaction.getFee().toNumberOfCoins());
		fee.setBorder( null );
		fee.setOpaque( false );
		fee.setEditable( false );
		p.add(fee);
		form.add(p);
		
		label = new JLabel("Application:", JLabel.RIGHT);
		form.add(label);
				
		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField app = new JTextField(36);
		app.setText(transaction.getApp());
		app.setBorder( null );
		app.setOpaque( false );
		app.setEditable( false );
		p.add(app);
		form.add(p);
		
		if(transaction.getKeywords() != null) {
			label = new JLabel("Keywords:", JLabel.RIGHT);
			form.add(label);
			form.add(new JPanel());
					
			p = new JPanel();
			p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
			form.add(p, "span 2, width 90%");
			
			JTextArea keywords = new JTextArea();
			keywords.setText(transaction.getKeywords().toString());
			keywords.setLineWrap(true);
			keywords.setWrapStyleWord(true);
			keywords.setOpaque( false );
			keywords.setEditable( false );
			p.add(keywords, BorderLayout.WEST);

		}
		
		String data = transaction.getExpiringData() == null ? "": transaction.getExpiringData().getData();
		
		if(!"".equals(data)) {
		
			label = new JLabel("Data:", JLabel.RIGHT);
			form.add(label);
			
			form.add(new JPanel());
			//form.add(new JPanel());
			
			//p = new JPanel(new BorderLayout());
			p = new JPanel();
			p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
			form.add(p, "span 2, width 90%");
	
			JTextArea expiringData = new JTextArea();
			expiringData.setLineWrap(true);
			expiringData.setWrapStyleWord(true);
			expiringData.setOpaque( false );
			expiringData.setEditable( false );

			p.add(expiringData, BorderLayout.WEST);
			
			
			expiringData.setText(data);
		}
		
		JFrame frame = main.getFrame();
		
		
		
		frame.getContentPane().removeAll();
		frame.getContentPane().add(scrollPane);
		frame.revalidate();
		frame.getContentPane().repaint();
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				scrollPane.getVerticalScrollBar().setValue(0);
			}
		});
	}

	public MempoolMouseAdapter(JTable table, List<Transaction> list, Main main) {
		this.table = table;
		this.list = list;
		this.main = main;
	}

}
