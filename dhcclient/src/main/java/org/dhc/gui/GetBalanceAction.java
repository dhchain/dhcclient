package org.dhc.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.dhc.gui.util.JTextFieldRegularPopupMenu;
import org.dhc.gui.util.SwingUtil;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.network.Network;
import org.dhc.util.Coin;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.StringUtil;
import org.dhc.util.ThreadExecutor;

public class GetBalanceAction extends AbstractAction implements Caller {

	private static final long serialVersionUID = 4798442956508802794L;
	private static final DhcLogger logger = DhcLogger.getLogger();

	private JTextField jAddress;
	private String address;
	private Main main;

	public GetBalanceAction(JTextField address, Main main) {
		putValue(NAME, "Get Balance");
		putValue(SHORT_DESCRIPTION, "Get Balance Action");
		this.jAddress = address;
		this.main = main;
	}
	
	public void showBalance(String balance) {

		logger.info("Address {} has balance {}", address,  balance);

		JPanel form = new JPanel(new BorderLayout());

		JPanel labelPanel = new JPanel(new GridLayout(2, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(2, 1));
		form.add(labelPanel, BorderLayout.WEST);
		form.add(fieldPanel, BorderLayout.CENTER);

		JLabel label = new JLabel(
				"DHC Address on shard " + new DhcAddress(address).getBinary(Network.getInstance().getPower()) + ":",
				JLabel.RIGHT);
		label.setBorder(new EmptyBorder(5, 5, 5, 5));
		labelPanel.add(label);

		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField textField = new JTextField(30);
		textField.setText(address);
		JTextFieldRegularPopupMenu.addTo(textField);
		p.add(textField);
		fieldPanel.add(p);

		

		label = new JLabel("Balance:", JLabel.RIGHT);
		label.setBorder(new EmptyBorder(5, 5, 5, 5));
		labelPanel.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		textField = new JTextField(30);
		textField.setText(balance + " coins");
		JTextFieldRegularPopupMenu.addTo(textField);
		p.add(textField);
		fieldPanel.add(p);

		JFrame frame = main.getFrame();
		frame.getContentPane().removeAll();
		frame.getContentPane().add(form, BorderLayout.NORTH);
		frame.revalidate();
		frame.getContentPane().repaint();
	}

	public void actionPerformed(ActionEvent e) {
		address = StringUtil.trim(jAddress.getText());
		if(!CryptoUtil.isDhcAddressValid(address)) {
			JOptionPane.showMessageDialog(main.getFrame(), "DHC Address is not valid");
			return;
		}
		
		SwingUtil.showWait(main);
		
		ThreadExecutor.getInstance().execute(new DhcRunnable("GetBalanceAction") {
			public void doRun() {
				
				Coin balance = DhcLiteHelper.getBalance(new DhcAddress(address));
				if(balance == null) {
					log("Could not get balance");
					return;
				}
				showBalance(balance.toNumberOfCoins());
				
			}
		});
		
	}

	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}



}
