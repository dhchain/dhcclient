package org.dhc.gui.promote;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.dhc.blockchain.SendTransactionMessage;
import org.dhc.blockchain.Transaction;
import org.dhc.blockchain.TransactionOutput;
import org.dhc.gui.Caller;
import org.dhc.gui.Main;
import org.dhc.gui.util.SwingUtil;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.network.Network;
import org.dhc.util.Coin;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.Listeners;
import org.dhc.util.StringUtil;
import org.dhc.util.ThreadExecutor;

public class JoinAction extends AbstractAction implements Caller {

	private static final long serialVersionUID = -5614773311159436840L;
	private static final DhcLogger logger = DhcLogger.getLogger();
	
	private Main main;
	private JTextField addressField;
	private JTextField amountField;

	public JoinAction(JTextField addressField, JTextField amountField, Main main) {
		putValue(NAME, "Join");
		putValue(SHORT_DESCRIPTION, "Join Action");
		this.main = main;
		this.addressField = addressField;
		this.amountField = amountField;
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		ThreadExecutor.getInstance().execute(new DhcRunnable("JoinAction") {
			public void doRun() {
				process();
			}
		});
	}
	
	private void process() {
		
		Set<Transaction> transactions =  DhcLiteHelper.getTransactionsForApp();
		if(transactions != null && !transactions.isEmpty()) {
			log("You already joined a team");
			return;
		}
		
		
		DhcAddress address = new DhcAddress(StringUtil.trim(addressField.getText()));
		if(!address.isDhcAddressValid()) {
			log("Please enter valid DHC Address");
			return;
		}
		if(address.equals(DhcAddress.getMyDhcAddress())) {
			log("You cannot join to your own DHC Address");
			return;
		}
		Coin amount;
		try {
			amount = Coin.ONE.multiply(Double.parseDouble(StringUtil.trim(amountField.getText())));
			if(amount.lessOrEqual(Coin.ZERO)) {
				log("Amount cannot be negative");
				return;
			}
		} catch (Exception e) {
			log("Please enter valid amount");
			return;
		}

		Coin balance = DhcLiteHelper.getBalance(DhcAddress.getMyDhcAddress());
		if(balance.less(amount)) {
			log("Your balance is less than entered amount");
			return;
		}
		
		final JLabel label = SwingUtil.showWait(main);

		JoinInfo joinInfo = DhcLiteHelper.getJoinInfo(address, amount);
		if(joinInfo == null) {
			label.setText("Could not retrieve joinInfo for " + address);
			return;
		}
		split(joinInfo, amount);
		label.setText("<html>You joined promotion with peer " + address + " <br>for amount " + amount.toNumberOfCoins() + "</html>");
		logger.info("You joined promotion with peer " + address);
	}
	
	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}
	
	private void split(JoinInfo joinInfo, Coin amount) {
		if(!joinInfo.isValid()) {
			logger.info("joinInfo.isValid() returned false");
			return;
		}
		try {
			Map<Integer, JoinInfoItem> items = joinInfo.getItems();
			List<Coin> list = new ArrayList<>();
			list.add(joinInfo.getFraction(amount, joinInfo.getAmount()));
			for(JoinInfoItem item: items.values()) {
				list.add(joinInfo.getFraction(amount, item.getAmount()));
			}
			Transaction transaction = new Transaction();
			Set<TransactionOutput> outputs = DhcLiteHelper.getTransactionOutputs();
			transaction.createSplitOutputsTransaction(DhcAddress.getMyDhcAddress(), Coin.ZERO, outputs, list.toArray(new Coin[0]));
			Listeners.getInstance().addEventListener(JoinTransactionEvent.class, new JoinTransactionEventListener(amount, transaction, joinInfo));
			Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
			Listeners.getInstance().sendEvent(new JoinTransactionEvent(transaction));
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

}
