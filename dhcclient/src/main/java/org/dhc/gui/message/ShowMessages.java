package org.dhc.gui.message;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import org.dhc.gui.Caller;
import org.dhc.gui.Main;
import org.dhc.gui.util.SwingUtil;
import org.dhc.gui.util.TableColumnAdjuster;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.lite.SecureMessage;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.ThreadExecutor;
import org.dhc.util.Wallet;


public class ShowMessages extends AbstractAction implements Caller {

	private static final long serialVersionUID = 4036313657721664495L;
	private static final DhcLogger logger = DhcLogger.getLogger();
	
	private Main main;
	
	public ShowMessages(Main main) {
		putValue(NAME, "Show Messages");
		putValue(SHORT_DESCRIPTION, "Show Messages");
		this.main = main;
	}
	
	
	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}

	public void actionPerformed(ActionEvent actionEvent) {
		
		final JLabel label = SwingUtil.showWait(main);
		
		final Wallet wallet = Wallet.getInstance();

		ThreadExecutor.getInstance().execute(new DhcRunnable("ShowMessages") {
			public void doRun() {

				List<SecureMessage> list = DhcLiteHelper.getSecureMessages(DhcAddress.getMyDhcAddress());
				
				if(list == null) {
					label.setText("Failed to retrieve messages. Please try again");
					return;
				}
				
				String dhcAddress = wallet.getDhcAddress().getAddress();
				logger.debug("found # of messages: {} for {}", list.size(), dhcAddress);
				
				JPanel form = new JPanel();
				form.setLayout(new BoxLayout(form, BoxLayout.Y_AXIS));
				
				JLabel label = new JLabel("Messages for " + dhcAddress);
				label.setAlignmentX(JLabel.LEFT_ALIGNMENT);
				label.setBorder(new EmptyBorder(5,5,5,5));
				form.add(label);
				
				if(list.size() != 0) {
					SecureMessageTableModel model = new SecureMessageTableModel(list);
					JTable table = new JTable(model);

					table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
					TableColumnAdjuster tca = new TableColumnAdjuster(table);
					tca.adjustColumns();
					table.addMouseListener(new MessageMouseAdapter(table, list, main));

					JScrollPane scroll = new JScrollPane (table);
					scroll.setBorder(null);
					form.add(scroll);
				} else {
					label.setText("No messages found for " + dhcAddress);
				}
				
				
				JFrame frame = main.getFrame();
				frame.getContentPane().removeAll();
				frame.getContentPane().add(form);
				frame.revalidate();
				frame.getContentPane().repaint();
			}
		});
	}

	

}
