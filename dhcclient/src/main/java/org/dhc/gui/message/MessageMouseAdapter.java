package org.dhc.gui.message;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import org.dhc.gui.Main;
import org.dhc.lite.SecureMessage;
import org.dhc.util.Base58;
import org.dhc.util.DhcLogger;
import org.dhc.util.Encryptor;
import org.dhc.util.StringUtil;
import org.dhc.util.Wallet;

public class MessageMouseAdapter extends MouseAdapter {
	
	private static final DhcLogger logger = DhcLogger.getLogger();
	private static final Encryptor encryptor = new Encryptor();
	
	private JTable table;
	private List<SecureMessage> list;
	private Main main;
	
	public void mouseClicked(MouseEvent evt) {
        int row = table.rowAtPoint(evt.getPoint());
        int col = table.columnAtPoint(evt.getPoint());
        if (row >= 0 && col >= 0) {
            doit(list.get(row));
        }
    }
	
	private JPanel showBackButton() {
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		final JButton btnSubmit = new JButton();
		btnSubmit.setAction(new ShowMessages(main));
		btnSubmit.setText("Back");
		
		main.getFrame().getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0), "backButton");

		main.getFrame().getRootPane().getActionMap().put("backButton", new AbstractAction() {
			private static final long serialVersionUID = 4946947535624344910L;

			public void actionPerformed(ActionEvent actionEvent) {
				btnSubmit.doClick();
				main.getFrame().getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).clear();
				main.getFrame().getRootPane().getActionMap().clear();
			}
		});
		
		p.add(btnSubmit);
		return p;
	}

	private void doit(SecureMessage secureMessage) {
		
		JFrame frame = main.getFrame();
		frame.getContentPane().removeAll();
		
		JPanel form = new JPanel(new BorderLayout());
		frame.getContentPane().add(form, BorderLayout.NORTH);
		
		JPanel p = showBackButton();
		
		Wallet wallet = Wallet.getInstance();;
		
		JButton btnReply = new JButton();
		String replyTo = secureMessage.getSenderDhcAddress();
		if(wallet.getDhcAddress().getAddress().equals(replyTo)) {
			replyTo = secureMessage.getRecipient();
		}
		SendMessage sendMessage = new SendMessage(main, replyTo);
		btnReply.setAction(sendMessage);
		btnReply.setText("Reply");
		p.add(btnReply);
		
		form.add(p, BorderLayout.NORTH);
		
		JPanel labelPanel = new JPanel(new GridLayout(9, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(9, 1));
		form.add(labelPanel, BorderLayout.WEST);
		form.add(fieldPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("Identifier:", JLabel.RIGHT);
		labelPanel.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField identifier = new JTextField(36);
		identifier.setText(secureMessage.getTransactionId());
		identifier.setBorder( null );
		identifier.setOpaque( false );
		identifier.setEditable( false );
		p.add(identifier);
		fieldPanel.add(p);
		
		label = new JLabel("Sender:", JLabel.RIGHT);
		labelPanel.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField address = new JTextField(36);
		address.setText(secureMessage.getSenderDhcAddress());
		address.setBorder( null );
		address.setOpaque( false );
		address.setEditable( false );
		p.add(address);
		fieldPanel.add(p);
		
		label = new JLabel("Recipient:", JLabel.RIGHT);
		labelPanel.add(label);
		
		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField recipient = new JTextField(36);
		recipient.setText(secureMessage.getRecipient());
		recipient.setBorder( null );
		recipient.setOpaque( false );
		recipient.setEditable( false );
		p.add(recipient);
		fieldPanel.add(p);
		
		
		label = new JLabel("Value:", JLabel.RIGHT);
		labelPanel.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField amount = new JTextField(36);
		amount.setText(secureMessage.getValue().toNumberOfCoins());
		amount.setBorder( null );
		amount.setOpaque( false );
		amount.setEditable( false );
		p.add(amount);
		fieldPanel.add(p);
		
		label = new JLabel("Fee:", JLabel.RIGHT);
		labelPanel.add(label);
				
		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField fee = new JTextField(36);
		fee.setText(secureMessage.getFee().toNumberOfCoins());
		fee.setBorder( null );
		fee.setOpaque( false );
		fee.setEditable( false );
		p.add(fee);
		fieldPanel.add(p);
		

		label = new JLabel("Date:", JLabel.RIGHT);
		labelPanel.add(label);
		
		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField date = new JTextField(36);
		date.setText(new Date(secureMessage.getTimeStamp()).toString());
		date.setBorder( null );
		date.setOpaque( false );
		date.setEditable( false );
		p.add(date);
		fieldPanel.add(p);
		
		label = new JLabel("Expire:", JLabel.RIGHT);
		labelPanel.add(label);
		
		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField expire = new JTextField(36);
		expire.setText(new Date(secureMessage.getTimeStamp() + secureMessage.getExpire() * 60000).toString());
		expire.setBorder( null );
		expire.setOpaque( false );
		expire.setEditable( false );
		p.add(expire);
		fieldPanel.add(p);
		
		
		label = new JLabel("Subject:", JLabel.RIGHT);
		labelPanel.add(label);

		p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField subject = new JTextField(36);
		subject.setBorder( null );
		subject.setOpaque( false );
		subject.setEditable( false );
		p.add(subject);
		fieldPanel.add(p);

		label = new JLabel("Body:", JLabel.RIGHT);
		labelPanel.add(label);
		
		p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		main.getFrame().getContentPane().add(p);

		JTextArea expiringData = new JTextArea();
		expiringData.setLineWrap(true);
		expiringData.setWrapStyleWord(true);
		expiringData.setOpaque( false );
		expiringData.setEditable( false );
		
		
		JScrollPane scroll = new JScrollPane (expiringData);
		scroll.setBorder(null);
		p.add(scroll);
		
		try {
			String str = StringUtil.trimToNull(secureMessage.getText());
			
			if(str != null && secureMessage.getRecipient().equals(wallet.getDhcAddress().getAddress())) {
				str = new String(encryptor.decryptAsymm(Base58.decode(str), wallet.getPrivateKey()), StandardCharsets.UTF_8);
				int index = str.indexOf("\n");
				if(index == -1) {
					subject.setText(str);
				} else {
					subject.setText(str.substring(0, index));
					expiringData.setText(str.substring(index + 1));
				}
				sendMessage.setSubject("RE " + subject.getText());
			}
		} catch (GeneralSecurityException e) {
			logger.error(e.getMessage(), e);
		}

		
		
		
		frame.revalidate();
		frame.getContentPane().repaint();
		expiringData.setCaretPosition(0);
	}

	public MessageMouseAdapter(JTable table, List<SecureMessage> list, Main main) {
		this.table = table;
		this.list = list;
		this.main = main;
	}

}
