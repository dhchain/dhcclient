package org.dhc.gui.message;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.dhc.gui.Caller;
import org.dhc.gui.Main;
import org.dhc.gui.util.KeyValue;
import org.dhc.gui.util.SwingUtil;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.util.Coin;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.StringUtil;
import org.dhc.util.ThreadExecutor;


public class SendMessageAction extends AbstractAction implements Caller {

	private static final long serialVersionUID = 4798442956508802794L;
	private static final DhcLogger logger = DhcLogger.getLogger();
	
	private Main main;
	
	private JTextField jaddress;
	private JTextField jfee;
	private JComboBox<KeyValue> jexpire;
	private JTextField jsubject;
	private JTextArea jexpiringData;

	private String receiverDhcAddress;
	private String fee; 
	private String expire;
	private String subject;
	private String expiringData;
	

	public SendMessageAction(Main main, JTextField address, JTextField fee, JComboBox<KeyValue> expire, JTextField subject, JTextArea expiringData) {
		putValue(NAME, "Send Secure Message");
		putValue(SHORT_DESCRIPTION, "Send SecureS Message Action");
		this.main = main;
		
		jaddress = address;
		jfee = fee;
		jexpire = expire;
		jexpiringData = expiringData;
		this.jsubject = subject;
	}
	
	private boolean validate() {
		if(!CryptoUtil.isDhcAddressValid(receiverDhcAddress)) {
			return false;
		}
		try {
			Long.parseLong(fee);
			if(subject != null) {
				Long.parseLong(expire);
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	private void load() {
		fee = StringUtil.trim(jfee.getText());
		expire = StringUtil.trim(((KeyValue) jexpire.getSelectedItem()).getValue());
		subject = StringUtil.trim(jsubject.getText());
		expiringData = StringUtil.trim(jexpiringData.getText());
		receiverDhcAddress = StringUtil.trim(jaddress.getText());
	}

	public void actionPerformed(ActionEvent ae) {
		load();
		
		if(!validate()) {
			log("Please verify inputs");
			return;
		}
		
		final JLabel label = SwingUtil.showWait(main);
		
		ThreadExecutor.getInstance().execute(new DhcRunnable("SendTransactionAction") {
			public void doRun() {
				
				String result = DhcLiteHelper.sendSecureMessage(new Coin(Integer.parseInt(fee)), receiverDhcAddress, subject, Long.parseLong(expire), expiringData);
				logger.info(result);
				label.setText(result);
			}
		});
		
	}

	public void log(String message) {
		JOptionPane.showMessageDialog(main.getFrame(), message);
	}



}
