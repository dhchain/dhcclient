DHC Client is a light client for Distributed Hashchain network

You can read more about the Distributed Hash Chain network by reviewing the link below:

https://github.com/dhchain/dhc

DHC Client functionality includes the following:

* Checking balance of any address on any shard in DHC.
* Sending coins between DHC addresses.

DHC Client does not require downloading of blockchain and can be installed and started quickly; most importantly, DHC Client uses less resources on your system. 

Installation instructions:

1. Install Java version 8 or 9
2. Download installer from <a href="http://download.dhcne.org/drive/dhc-lite-client-installer.jar" target="_blank" download="dhc-client-installer">Download</a>
3. If you already have key.csv file with existing keys you can just copy it to config folder, otherwise it will create new key. 
4. Start it by running dhcgui.sh or dhcgui.bat depending on your operating system.

It will ask you to enter passphrase and confirm it. In case you have existing key it will ask you to enter passphrase for it.

<BR />

![Passphrase](dhcclient/media/image001.png)

<BR />

It will then start network by connecting to full peers specified in peers.config

You can check balance of any address.

<BR />

![Balance](dhcclient/media/image002.png)

<BR />

Send transaction

<BR />

![Transaction](dhcclient/media/image003.png)